from flask import Flask
import requests

app = Flask(__name__)

@app.route("/")
def index():
    try:
        response = requests.get("http://api2:5000/")
        if response.status_code == 200:
            return f"1{response.text}"
        else:
            return "1"
    except:
        return "1"        

if __name__ == "__main__":
    app.run()