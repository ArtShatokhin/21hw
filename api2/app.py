from flask import Flask
import requests

app = Flask(__name__)

@app.route("/")
def index():
    try:
        response = requests.get("http://api3:5000/")
        if response.status_code == 200:
            return f"2{response.text}"
        else:
            return "2"
    except:
        return "2"        

if __name__ == "__main__":
    app.run()